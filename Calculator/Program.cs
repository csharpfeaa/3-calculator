﻿using System;

namespace Calculator
{
    internal class Program
    {
         static void Main(string[] args)
        {
            Console.Write("Introduceti a: ");

            if (!double.TryParse(Console.ReadLine(), out double a))
            {
                Console.WriteLine("Numarul introdus nu este valid.");
                Console.ReadKey();
                return;
            }

            Console.Write("Introduceti b: ");

            if (!double.TryParse(Console.ReadLine(), out double b))
            {
                Console.WriteLine("Numarul introdus nu este valid.");
                Console.ReadKey();
                return;
            }

            // preluam operatiunea
            Console.Write("Operatiunea (+, -, *, /): ");

            string operatiune = Console.ReadLine();

            if (operatiune == "+")
            {
                Console.WriteLine("a + b = " + (a + b));
            }
            else if (operatiune == "-")
            {
                Console.WriteLine("a - b = " + (a - b));
            }
            else if (operatiune == "*")
            {
                Console.WriteLine("a * b = " + (a * b));
            }
            else if (operatiune == "/")
            {
                if (b == 0)
                    Console.WriteLine("Operatiune imposibila");
                else
                    Console.WriteLine("a / b = " + (a / b));
            }
            else
            {
                Console.WriteLine("Operatiune invalida");
            }

            Console.ReadKey();
        }

        static void switchExample(string operatiune, double a, double b)
        {
            switch (operatiune)
            {
                case "+":
                    Console.WriteLine("a + b = " + (a + b));
                    break;
                case "-":
                    Console.WriteLine("a - b = " + (a - b));
                    break;
                case "*":
                    Console.WriteLine("a * b = " + (a * b));
                    break;
                case "/":
                    if (b == 0)
                        Console.WriteLine("Operatiune imposibila");
                    else
                        Console.WriteLine("a / b = " + (a / b));

                    break;
                default:
                    Console.WriteLine("Operatiune invalida");
                    break;
            }
        }
    }
}